//
//

#include <assert.h>
#include "bst.h"

static int __child_r(int i) {
    return 2*(i+1)-1+1;
}

static int __child_l(int i) {
    return 2*(i+1)-1;
}

void init_tree(bst *head) {
    int i;
    FILE *f;
    telefonas t;
    for (i=0; i<MAX_ELEM; i++)
        head->elem[i].id = EMPTY;
    f = fopen(DUOM_FAILAS, "r");
    assert(f != NULL);
    while(!feof(f)) {
        if(!nuskaityti(f, &t))
            break;
        insert(head, t);
    }
    fclose(f);
}

static void __insert(bst *head, telefonas *t, int idx) {
    assert(idx < MAX_ELEM);
    if (head->elem[idx].id == EMPTY) {
        memcpy(&(head->elem[idx]), t, sizeof(head->elem[idx]));
        return;
    }
    assert(head->elem[idx].id != t->id);
    if (head->elem[idx].id < t->id) {
        __insert(head, t, __child_r(idx));
    } else {
        __insert(head, t, __child_l(idx));
    }
    return;
}

void insert(bst *head, telefonas t) {
    __insert(head, &t, 0);
}

static void __print_infix(bst *head, int idx) {
    assert (idx < MAX_ELEM);
    if (head->elem[idx].id == EMPTY)
        return;
    __print_infix(head, __child_l(idx));
    parasyti(head->elem[idx]);
    __print_infix(head, __child_r(idx));
}

static void __print_prefix(bst *head, int idx) {
    assert (idx < MAX_ELEM);
    if (head->elem[idx].id == EMPTY)
        return;
    parasyti(head->elem[idx]);
    __print_prefix(head, __child_l(idx));
    __print_prefix(head, __child_r(idx));
}
static void __print_postfix(bst *head, int idx) {
    assert (idx < MAX_ELEM);
    if (head->elem[idx].id == EMPTY)
        return;
    __print_postfix(head, __child_l(idx));
    __print_postfix(head, __child_r(idx));
    parasyti(head->elem[idx]);
    return;
}

void print(bst *head, order o) {
    switch (o) {
        case infix: __print_infix(head, 0);
            break;
        case postfix: __print_postfix(head, 0);
            break;
        case prefix: __print_prefix(head, 0);
            break;
    }
}

static void __search(bst *head, int key, int idx) {
    if (head->elem[idx].id == EMPTY) {
        fprintf(stderr, "Nerasta\n");
        return;
    }
    parasyti(head->elem[idx]);
    if (head->elem[idx].id == key) {
        return;
    } else if (head->elem[idx].id > key) {
        __search(head, key, __child_l(idx));
    } else {
        __search(head, key, __child_r(idx));
    }
}

void search(bst *head, int key) {
    __search(head, key, 0);
}

static void __delete_helper(bst * head, int idx_from, int idx_to) {
    if(head->elem[idx_from].id == EMPTY) return;
    memcpy(&(head->elem[idx_to]), &(head->elem[idx_from]), sizeof(head->elem[idx_from]));
    head->elem[idx_from].id = EMPTY;
    __delete_helper(head, __child_l(idx_from), __child_l(idx_to));
    __delete_helper(head, __child_r(idx_from), __child_r(idx_to));
}

static void __delete(bst *head, int key, int idx) {
    int idx_rl;
    if (head->elem[idx].id == EMPTY) {
        fprintf(stderr, "Nerasta\n");
        return;
    }
    if (head->elem[idx].id == key) {
        if (head->elem[__child_l(idx)].id == EMPTY) {
            if (head->elem[__child_r(idx)].id == EMPTY) { // nėra vaikų
                head->elem[idx].id = EMPTY;
            } else {
                __delete_helper(head, __child_r(idx), idx); // yra tik dešinysis vaikas
            }
        } else  {
            if (head->elem[__child_r(idx)].id == EMPTY) { // yra tik kairysis vaikas
                __delete_helper(head, __child_l(idx), idx);
            } else {
                idx_rl = __child_r(idx); // yra abu vaikai
                while (head->elem[__child_l(idx_rl)].id != EMPTY) { // randamas dešiniojo pomedžio kairiausias vaikas
                    idx_rl = __child_l(idx_rl);
                }
                memcpy(&(head->elem[idx]), &(head->elem[idx_rl]), sizeof(head->elem[idx]));
                __delete(head, head->elem[idx_rl].id, idx_rl);


            }
        }
    } else if (head->elem[idx].id > key) {
        __delete(head, key, __child_l(idx));
    } else if (head->elem[idx].id < key) {
        __delete(head, key, __child_r(idx));
    }
}

void delete(bst *head, int key) {
    __delete(head, key, 0);
}
