#include "telefonas.h"

#ifndef AGNEI_BST_BST_H
#define AGNEI_BST_BST_H
#define EMPTY -1
#define MAX_ELEM 10000
#define DUOM_FAILAS "duom.txt"

typedef enum{infix, prefix, postfix} order;

typedef struct{
    telefonas elem [MAX_ELEM];
} bst;

void init_tree(bst *);
void insert(bst *, telefonas);
void delete(bst *, int);
void search(bst *, int);
void print(bst * , order);

#endif //AGNEI_BST_BST_H
