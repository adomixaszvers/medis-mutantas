//
//

#include "telefonas.h"

int nuskaityti(FILE *file, telefonas *t) {
    char tn;
    int nuskaityta;
    nuskaityta = fscanf(file, "%20s %20s %f %d %c %d", t->gamintojas, t->modelis, &(t->kaina), &(t->isleistas), &tn, &(t->id));
    t->garantija = (tn == 'T') ? YRA: NERA;
    return nuskaityta == 6? 1: 0;
}

void parasyti(telefonas t) {
    char tn;
    tn = (t.garantija == YRA)? 'T': 'N';
    printf("%s %s %.2f %d %c %d\n", t.gamintojas, t.modelis, t.kaina, t.isleistas, tn, t.id);
}
