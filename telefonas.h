#include <stdio.h>
#include <string.h>

#ifndef AGNEI_BST_TELEFONAS_H
#define AGNEI_BST_TELEFONAS_H

typedef enum {NERA=0, YRA=1} teiginys;

typedef struct {
    char gamintojas[21];
    char modelis[21];
    float kaina;
    int isleistas;
    teiginys garantija;
    int id;
} telefonas;

int nuskaityti(FILE *, telefonas *);
void parasyti(telefonas);

#endif //AGNEI_BST_TELEFONAS_H
