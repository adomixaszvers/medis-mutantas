#include "stdio.h"
#include "telefonas.h"
#include "bst.h"

int main() {
    char tn;
    int id;
    int cont = 1;
    telefonas t;
    bst head;
    init_tree(&head);
    while(cont) {
        puts("==============================");
        puts("1. Insert(head, x)");
        puts("2. Delete(head, x)");
        puts("3. Search(head, x)");
        puts("4. Print(head, infix)");
        puts("5. Print(head, prefix)");
        puts("6. Print(head, postfix)");
        puts("Bet kas kitas uzbaigs programa");
        printf("Jusu pasirinkimas: ");
        scanf("%c", &tn);
        while(getchar() != '\n');
        switch (tn) {
            case '1':
                printf("Telefonas: ");
                if(!nuskaityti(stdin, &t)) {
                    while(getchar() != '\n');
                    puts("Netinkami duomenys");
                } else {
                    while(getchar() != '\n');
                    insert(&head, t);
                }
                break;
            case '2':
                printf("Id: ");
                if(scanf("%d", &id) != 1) {
                    fprintf(stderr, "Netinkamas Id");
                    continue;
                }
                while(getchar() != '\n');
                delete(&head, id);
                break;
            case '3':
                printf("Id: ");
                if(scanf("%d", &id) != 1) {
                    fprintf(stderr, "Netinkamas Id");
                    continue;
                }
                while(getchar() != '\n');
                search(&head, id);
                break;
            case '4':
                print(&head, infix);
                break;
            case '5':
                print(&head, prefix);
                break;
            case '6':
                print(&head, postfix);
                break;
            default:
                cont = 0;
        }
    }
    return 0;
}